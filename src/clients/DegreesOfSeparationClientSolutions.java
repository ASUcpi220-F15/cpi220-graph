package clients;

import datastructures.Bag;
import graph.BreadthFirstPaths;
import graph.CC;
import graph.DepthFirstPaths;
import graph.Graph;
import graph.SymbolGraph;
import io.StdIn;
import io.StdOut;

public class DegreesOfSeparationClientSolutions 
{
	public static void printAllAtDist(String source, SymbolGraph sg)
	{	int s = sg.index(source);
    	Graph G = sg.G();
    	BreadthFirstPaths bfs = new BreadthFirstPaths(G, s);
		 
    	System.out.println("Type in a distance: ");
	    while (!StdIn.isEmpty())
	    {
	        	int dist = new Integer(StdIn.readLine()).intValue();
	        	
	        	//FILL IN REMAINDER OF METHOD HERE
	        	Bag<Integer> distBag = new Bag<Integer>();
	        	
	        	for (int i = 0; i < G.V(); i++)
	        	{
	        		if (bfs.distTo(i) == dist)
	        			distBag.add(i);
	        	}
	        	if (distBag.size() <= 0)
	        		System.out.println("There are no nodes at that distance");
	        	for (int v : distBag)
	        	{
	        		System.out.println(sg.name(v));
	        	}
	        	System.out.println("Type in a distance: ");
	    }
	}
	
	public static void getNodesConnected(SymbolGraph sg, String source)
	{
		Graph G = sg.G();
		int s = sg.index(source);
		CC connectedComponents = new CC(G);
		System.out.println(connectedComponents.size(s) + " - " + G.V() + " - " + connectedComponents.count());
	}
	
	public static void degreesOfSeparationDFS(String source, SymbolGraph sg)
	{
		int s = sg.index(source);
        Graph G = sg.G();
		DepthFirstPaths bfs = new DepthFirstPaths(G, s);
		
        System.out.println("Type in the name of a second node: ");

	    while (!StdIn.isEmpty()) 
	    {
	    	String sink = StdIn.readLine();
            if (sg.contains(sink)) {
                int t = sg.index(sink);
                if (bfs.hasPathTo(t)) {
                    for (int v : bfs.pathTo(t)) {
                        StdOut.println("   " + sg.name(v));
                    }
                }
                else {
                    StdOut.println("Not connected");
                }
            }
            else {
                StdOut.println("   Not in database.");
            }
        }
	}
	
	public static void degreesOfSeparationBFS(String source, SymbolGraph sg)
	{
		int s = sg.index(source);
        Graph G = sg.G();
		BreadthFirstPaths bfs = new BreadthFirstPaths(G, s);
		
		System.out.println("Type in the name of a second node:");
        while (!StdIn.isEmpty()) 
        {
            String sink = StdIn.readLine();
            if (sg.contains(sink)) {
                int t = sg.index(sink);
                if (bfs.hasPathTo(t)) {
                    for (int v : bfs.pathTo(t)) {
                        StdOut.println("   " + sg.name(v));
                    }
                }
                else {
                    StdOut.println("Not connected");
                }
            }
            else {
                StdOut.println("   Not in database.");
            }
        }	
	}
	
    public static void main(String[] args) 
    {
    	 String filename  = "src/data/movies.txt";
         String delimiter = "/";

         // StdOut.println("Source: " + source);
         SymbolGraph sg = new SymbolGraph(filename, delimiter);
         System.out.println("Type in the name of a node: ");

         while (StdIn.hasNextLine()) 
     	{
     		String source = StdIn.readLine();
     		if (!sg.contains(source)) {
    			StdOut.println(source + " not in database.");
    		}
     		else
     		{
     			//CALL METHODS HERE
     			DegreesOfSeparationClientSolutions.degreesOfSeparationDFS(source, sg);
     		}
            System.out.println("Type in the name of a node: ");

     	}
    }
}
